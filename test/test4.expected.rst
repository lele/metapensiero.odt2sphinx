This is `Marco's`__ demo document: the initial ``T`` has its own style...

These paths are used to find the footprints library files (.mod), equivalence files
(.equ), and 3D model files (.wrl) used by CvPcb.

__ https://bitbucket.org/cdevienne/odt2sphinx/issues/5/change-in-style-make-text-wrap-round

First section
=============

As said, this is `Marco's`__ demo.

__ https://bitbucket.org/cdevienne/odt2sphinx/issues/5/change-in-style-make-text-wrap-round
